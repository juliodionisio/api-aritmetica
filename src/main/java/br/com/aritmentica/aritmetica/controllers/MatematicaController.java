package br.com.aritmentica.aritmetica.controllers;

import br.com.aritmentica.aritmetica.DTOs.EntradaDTO;
import br.com.aritmentica.aritmetica.DTOs.RespostaDTO;
import br.com.aritmentica.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){
        validarEntrada(entradaDTO);

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;


    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO){

        validarEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;

    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO){
        validarEntrada(entradaDTO);

        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;

    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO){
        int anterior = 9999999;
        for (int n: entradaDTO.getNumeros()){

            if(n > anterior){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é possível efetuar uma divisão por um número maior");
            }

            anterior = n;
        }
        validarEntrada(entradaDTO);
        RespostaDTO resposta = matematicaService.divisao(entradaDTO);
        return resposta;

    }


    private void validarEntrada(EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros para serem somados");
        }

        for (int n: entradaDTO.getNumeros()){
            if(n < 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie somente números naturais");
            }
        }


    }
}
