package br.com.aritmentica.aritmetica.service;

import br.com.aritmentica.aritmetica.DTOs.EntradaDTO;
import br.com.aritmentica.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class MatematicaService {

    public MatematicaService() {
    }

    public RespostaDTO soma(EntradaDTO entrada){
        int numero = 0;
        for(int n: entrada.getNumeros()){
            numero += n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResposta(numero);

        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entrada){
        int numero = 0;
        for(int n: entrada.getNumeros()){
            numero -= n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResposta(numero);

        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entrada){
        int numero = 1;
        for(int n: entrada.getNumeros()){
            numero = numero * n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResposta(numero);

        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entrada){
        int anterior = -1;
        int numero = 0;
        for(int n: entrada.getNumeros()){

            if(anterior > 0){
                numero = anterior/n;
            }
            anterior = n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResposta(numero);

        return resposta;
    }


}
